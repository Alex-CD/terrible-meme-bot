package event

type ChannelMessage interface {
	ServerId() string
	AuthorId() string
	ChannelId() string
	MessageId() string
	Content() string
}
