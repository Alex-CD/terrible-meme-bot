package event

type ChannelEvent interface {
	ServerId() string
	AuthorId() string
	ChannelId() string
}
