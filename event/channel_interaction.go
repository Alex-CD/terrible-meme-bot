package event

type ChannelInteraction interface {
	ServerId() string
	AuthorId() string
	ChannelId() string
}
