package handlers_test

import (
	"gitlab.com/Alex-CD/terrible-meme-bot/command"
	"gitlab.com/Alex-CD/terrible-meme-bot/handlers"
	"gitlab.com/Alex-CD/terrible-meme-bot/mocks"
	"gitlab.com/Alex-CD/terrible-meme-bot/router"
	"testing"
)

type mockInteractionEvent struct{}

func (m *mockInteractionEvent) ServerId() string {
	return "someServerID"
}

func (m *mockInteractionEvent) AuthorId() string {
	return "someAuthorID"
}

func (m *mockInteractionEvent) ChannelId() string {
	return "someChannelID"
}

func TestInteractionHandler_Handle(t *testing.T) {
	stubLogger := mocks.NewStubLogger()
	mockMessageRepository := new(mocks.MessageRepository)
	mockVoiceRepository := new(mocks.VoiceRepository)
	mockInteractionRepository := new(mocks.InteractionRepository)
	mockCommand := new(mocks.ChannelCommand)
	mockCommandConstructor := func() command.ChannelCommand {
		return mockCommand
	}

	availableCommands := map[string]router.ChannelCommandDefinition{
		"testCommandName": {
			Description:        "test command description",
			HandlerConstructor: mockCommandConstructor,
		},
	}

	event := mockInteractionEvent{}

	mockCommand.
		On("Run", &event, mockMessageRepository, mockVoiceRepository, stubLogger).
		Return("some test response string")

	mockInteractionRepository.On("SendResponse", "some test response string", stubLogger).Return(nil)

	handler := handlers.NewInteractionHandler(mockMessageRepository, mockInteractionRepository, availableCommands)

	handler.Handle(&event, "testCommandName", mockVoiceRepository, stubLogger)

	mockCommand.AssertExpectations(t)
	mockInteractionRepository.AssertExpectations(t)

}
