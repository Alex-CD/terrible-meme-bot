package handlers_test

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/Alex-CD/terrible-meme-bot/handlers"
	"gitlab.com/Alex-CD/terrible-meme-bot/mocks"
	"testing"
)

type mockMessageEvent struct{}

func (m *mockMessageEvent) ServerId() string {
	return "someServerID"
}

func (m *mockMessageEvent) AuthorId() string {
	return "someAuthorID"
}

func (m *mockMessageEvent) ChannelId() string {
	return "someChannelID"
}

func (m *mockMessageEvent) MessageId() string {
	return "someMessageID"
}

func (m *mockMessageEvent) Content() string {
	return "!helloWorld"
}

func TestMessageHandler_Handle(t *testing.T) {
	stubLogger := mocks.NewStubLogger()
	mockMessageRepository := new(mocks.MessageRepository)
	mockVoiceRepository := new(mocks.VoiceRepository)
	mockCommand := new(mocks.ChannelCommand)

	mockRouter := new(mocks.MockMessageRouter)
	mockRouter.On("Route", mock.AnythingOfType("uint8"), "!helloWorld", stubLogger).Return(mockCommand)

	event := mockMessageEvent{}

	mockCommand.
		On("Run", &event, mockMessageRepository, mockVoiceRepository, stubLogger).
		Return("some test response string")

	mockMessageRepository.
		On("SendMessageInChannel", "some test response string", "someChannelID", stubLogger).
		Return("", nil)

	handler := handlers.NewMessageHandler(mockMessageRepository, mockRouter)

	handler.Handle(&event, mockVoiceRepository, stubLogger)

	mockCommand.AssertExpectations(t)
	mockMessageRepository.AssertExpectations(t)
}
