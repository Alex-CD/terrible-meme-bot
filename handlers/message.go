package handlers

import (
	"fmt"
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/message"
	"gitlab.com/Alex-CD/terrible-meme-bot/router"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
)

type MessageHandler interface {
	Handle(messageEvent event.ChannelMessage, voiceRepository voice.Repository, logger *slog.Logger)
}
type messageHandler struct {
	messageRepository message.Repository
	router            router.MessageRouter
}

func (m *messageHandler) Handle(messageEvent event.ChannelMessage, voiceRepository voice.Repository, logger *slog.Logger) {
	logger.Debug(
		fmt.Sprintf("Handling message with id %s on server with id %s", messageEvent.MessageId(), messageEvent.ServerId()))

	foundCommand := m.router.Route('!', messageEvent.Content(), logger)
	if foundCommand != nil {
		response := foundCommand.Run(messageEvent, m.messageRepository, voiceRepository, logger)

		_, err := m.messageRepository.SendMessageInChannel(response, messageEvent.ChannelId(), logger)
		if err != nil {
			logger.Error(fmt.Sprintf("Error sending command response message: %s", err.Error()))
		}
	}
}

func NewMessageHandler(messageRepository message.Repository, router router.MessageRouter) MessageHandler {
	return &messageHandler{
		messageRepository,
		router,
	}
}
