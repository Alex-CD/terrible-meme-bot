package handlers

import (
	"fmt"
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/interaction"
	"gitlab.com/Alex-CD/terrible-meme-bot/message"
	"gitlab.com/Alex-CD/terrible-meme-bot/router"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log"
	"log/slog"
)

type InteractionHandler interface {
	Handle(event event.ChannelInteraction, commandName string, voiceRepository voice.Repository, logger *slog.Logger)
}

type interactionHandler struct {
	messageRepository             message.Repository
	interactionResponseRepository interaction.Repository
	availableCommands             map[string]router.ChannelCommandDefinition
}

func (i *interactionHandler) Handle(event event.ChannelInteraction, commandName string, voiceRepository voice.Repository, logger *slog.Logger) {
	if foundCommand, ok := i.availableCommands[commandName]; ok {
		response := foundCommand.HandlerConstructor().Run(event, i.messageRepository, voiceRepository, logger)

		err := i.interactionResponseRepository.SendResponse(response, logger)
		if err != nil {
			logger.Error(fmt.Sprintf("error sending interaction response: %s", err.Error()))
		}

		return
	}

	log.Printf("Requested interaction '%s'handled but no command found!\n", commandName)
}

func NewInteractionHandler(messageRepository message.Repository, interactionResponseRepository interaction.Repository, availableCommands map[string]router.ChannelCommandDefinition) InteractionHandler {
	return &interactionHandler{
		messageRepository,
		interactionResponseRepository,
		availableCommands,
	}
}
