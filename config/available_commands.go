package config

import (
	"gitlab.com/Alex-CD/terrible-meme-bot/command"
	"gitlab.com/Alex-CD/terrible-meme-bot/router"
)

var AvailableChannelCommands = map[string]router.ChannelCommandDefinition{
	"ping": {
		Description:        "Replies with 'pong!'",
		HandlerConstructor: command.NewPing,
	},
	"help": {
		Description:        "Shows a list of available commands",
		HandlerConstructor: command.NewHelp,
	},
}
