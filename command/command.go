package command

import (
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/message"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
)

type ChannelCommand interface {
	Run(event event.ChannelEvent, messageRepository message.Repository, voiceRepository voice.Repository, logger *slog.Logger) string
}
