package command

import (
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/message"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
)

type Help struct {
}

func NewHelp() ChannelCommand { return &Help{} }

func (h Help) Run(_ event.ChannelEvent, _ message.Repository, _ voice.Repository, _ *slog.Logger) string {
	return "Available commands: !ping, !help"
}
