package command

import (
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/message"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
)

type Ping struct {
}

func NewPing() ChannelCommand {
	return &Ping{}
}

func (command Ping) Run(_ event.ChannelEvent, _ message.Repository, _ voice.Repository, _ *slog.Logger) string {
	return "Pong!"
}
