package command_test

import (
	"gitlab.com/Alex-CD/terrible-meme-bot/command"
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/mocks"
	"strings"
	"testing"
)

func TestHelp_Run(t *testing.T) {

	testHelp := command.NewHelp()
	stubChannelEvent := new(event.ChannelEvent)
	mockMessageRepository := new(mocks.MessageRepository)
	mockVoiceRepository := new(mocks.VoiceRepository)

	response := testHelp.Run(*stubChannelEvent, mockMessageRepository, mockVoiceRepository, nil)

	expectedResponse := "Available commands: !ping, !help"
	if strings.Compare(response, expectedResponse) != 0 {
		t.Fatalf(`Wanted %s, got %s`, expectedResponse, response)
	}

}
