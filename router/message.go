package router

import (
	"fmt"
	"gitlab.com/Alex-CD/terrible-meme-bot/command"
	"log/slog"
	"strings"
)

type ChannelCommandDefinition struct {
	Description        string
	HandlerConstructor func() command.ChannelCommand
}

type MessageRouter interface {
	Route(activationRune uint8, messageContent string, logger *slog.Logger) command.ChannelCommand
}

type messageRouter struct {
	triggerWords map[string]ChannelCommandDefinition
}

func NewMessageRouter(availableCommands map[string]ChannelCommandDefinition) MessageRouter {
	return &messageRouter{
		triggerWords: availableCommands,
	}
}

func (r messageRouter) Route(activationRune uint8, messageContent string, logger *slog.Logger) command.ChannelCommand {
	trimmedMessageContent := strings.TrimPrefix(messageContent, " ")

	if len(trimmedMessageContent) > 0 && trimmedMessageContent[0] == activationRune {
		firstWord := strings.Split(trimmedMessageContent[1:], " ")[0]
		foundCommand, ok := r.triggerWords[firstWord]
		if ok {
			logger.Debug(fmt.Sprintf("Message matched for command %s", firstWord))
			return foundCommand.HandlerConstructor()
		}
	}

	logger.Debug(fmt.Sprintf("No matching command found for message %s, prefix %s", messageContent, string(activationRune)))

	return nil
}
