package router_test

import (
	"gitlab.com/Alex-CD/terrible-meme-bot/command"
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/message"
	"gitlab.com/Alex-CD/terrible-meme-bot/mocks"
	"gitlab.com/Alex-CD/terrible-meme-bot/router"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
	"reflect"
	"testing"
)

type testCommand struct {
}

func (t testCommand) Run(_ event.ChannelEvent, _ message.Repository, _ voice.Repository, _ *slog.Logger) string {
	return ""
}

func testCommandConstructor() command.ChannelCommand {
	return testCommand{}
}
func anotherTestCommandConstructor() command.ChannelCommand {
	return testCommand{}
}

var availableCommands = map[string]router.ChannelCommandDefinition{
	"test": {
		Description:        "a test command",
		HandlerConstructor: testCommandConstructor,
	},
	"somethingElse": {
		Description:        "another test command",
		HandlerConstructor: anotherTestCommandConstructor,
	},
}

func TestMessageRouter_Route(t *testing.T) {

	tests := []struct {
		name           string
		input          string
		expectedOutput command.ChannelCommand
	}{
		{
			name:           "valid command",
			input:          "!test",
			expectedOutput: testCommand{},
		},
		{
			name:           "valid command with trailing space ",
			input:          "!test ",
			expectedOutput: testCommand{},
		},
		{
			name:           "valid command with leading and trailing exclamation mark",
			input:          " !test ",
			expectedOutput: testCommand{},
		},
		{
			name:           "character with only a space",
			input:          " ",
			expectedOutput: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			stubLogger := mocks.NewStubLogger()
			routerUnderTest := router.NewMessageRouter(availableCommands)

			foundCommand := routerUnderTest.Route('!', test.input, stubLogger)
			if !reflect.DeepEqual(foundCommand, test.expectedOutput) {
				t.Fatalf("expected %v, got %v", test.expectedOutput, routerUnderTest.Route('!', test.input, nil))
			}
		})
	}
}
