package voice

import "log/slog"

type Repository interface {
	GetVoiceChannel(serverID string, channelID string, logger *slog.Logger) (*Channel, error)
}

type Channel struct {
	Name    string
	Members []string
}
