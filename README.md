![Terrible Meme Bot Logo](/docs/images/logo.png)

A questionably helpful Discord bot.
Currently, it's a work in progress and is in the middle of a rewrite. Stay tuned.


Powered by Golang.


## Commands

`!ping` : Pong! :o 

`!help` : Not sure if this one needs explaining either.

More on the way soon™

## Getting started

Clone the repo, run `go run build`, and `go run start`.

You need two environment variables: `DISCORD_ID` and `DISCORD_TOKEN`.
You'll also need to grant your bot the privileged gateway intents on the discord applications portal.

## Contributing

Feel like adding something?
Please make sure your build compiles and passes linting checks before opening a pull request :)
