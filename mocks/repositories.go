package mocks

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
)

type MessageRepository struct {
	mock.Mock
}

func (m *MessageRepository) SendMessageInChannel(message string, channelId string, logger *slog.Logger) (messageId string, error error) {
	args := m.Called(message, channelId, logger)
	return args.Get(0).(string), args.Error(1)
}

func (m *MessageRepository) ReactWithEmoji(emojiId string, messageId string, channelId string, logger *slog.Logger) error {
	args := m.Called(emojiId, messageId, channelId, logger)
	return args.Error(0)
}

type VoiceRepository struct {
	mock.Mock
}

func (v *VoiceRepository) GetVoiceChannel(serverID string, channelID string, logger *slog.Logger) (*voice.Channel, error) {
	args := v.Called(serverID, channelID, logger)
	return args.Get(0).(*voice.Channel), args.Error(1)
}
