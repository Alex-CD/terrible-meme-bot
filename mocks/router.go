package mocks

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/Alex-CD/terrible-meme-bot/command"
	"log/slog"
)

type MockMessageRouter struct {
	mock.Mock
}

func (m *MockMessageRouter) Route(activationRune uint8, messageContent string, logger *slog.Logger) command.ChannelCommand {
	args := m.Called(activationRune, messageContent, logger)
	return args.Get(0).(command.ChannelCommand)
}
