package mocks

import (
	"github.com/stretchr/testify/mock"
	"log/slog"
)

type InteractionRepository struct {
	mock.Mock
}

func (m *InteractionRepository) SendResponse(message string, logger *slog.Logger) error {
	args := m.Called(message, logger)

	return args.Error(0)
}
