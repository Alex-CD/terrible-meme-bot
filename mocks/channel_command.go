package mocks

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/Alex-CD/terrible-meme-bot/event"
	"gitlab.com/Alex-CD/terrible-meme-bot/message"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
)

type ChannelCommand struct {
	mock.Mock
}

func (c *ChannelCommand) Run(event event.ChannelEvent, messageRepository message.Repository, voiceRepository voice.Repository, logger *slog.Logger) string {
	args := c.Called(event, messageRepository, voiceRepository, logger)
	return args.String(0)
}
