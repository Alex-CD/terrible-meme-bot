package mocks

import (
	"github.com/bwmarrin/discordgo"
	"github.com/stretchr/testify/mock"
)

type MockDiscordSession struct {
	mock.Mock
}

func (m *MockDiscordSession) ChannelMessageSend(channelID string, content string, options ...discordgo.RequestOption) (*discordgo.Message, error) {
	args := m.Called(channelID, content, options)
	return args.Get(0).(*discordgo.Message), args.Error(1)
}

func (m *MockDiscordSession) MessageReactionAdd(channelID, messageID, emojiID string, options ...discordgo.RequestOption) error {
	args := m.Called(channelID, messageID, emojiID, options)
	return args.Error(0)
}

func (m *MockDiscordSession) InteractionRespond(interaction *discordgo.Interaction, resp *discordgo.InteractionResponse, options ...discordgo.RequestOption) error {
	args := m.Called(interaction, resp, options)
	return args.Error(0)
}

func (m *MockDiscordSession) GetState() *discordgo.State {
	args := m.Called()
	return args.Get(0).(*discordgo.State)
}
