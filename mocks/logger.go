package mocks

import (
	"io"
	"log/slog"
)

func NewStubLogger() *slog.Logger {
	return slog.New(slog.NewTextHandler(io.Discard, nil))
}
