package discord_test

import (
	"errors"
	"github.com/bwmarrin/discordgo"
	"github.com/stretchr/testify/mock"
	"gitlab.com/Alex-CD/terrible-meme-bot/command"
	"gitlab.com/Alex-CD/terrible-meme-bot/discord"
	"gitlab.com/Alex-CD/terrible-meme-bot/mocks"
	"strings"
	"testing"
)

type mockSession struct {
	mock.Mock
}

func (m *mockSession) ChannelMessageSend(channelID string, content string, options ...discordgo.RequestOption) (*discordgo.Message, error) {
	args := m.Called(channelID, content, options)
	return args.Get(0).(*discordgo.Message), args.Error(1)
}

func (m *mockSession) MessageReactionAdd(channelID, messageID, emojiID string, options ...discordgo.RequestOption) error {
	args := m.Called(channelID, messageID, emojiID, options)
	return args.Error(0)
}

func (m *mockSession) InteractionRespond(interaction *discordgo.Interaction, resp *discordgo.InteractionResponse, options ...discordgo.RequestOption) error {
	args := m.Called(interaction, resp, options)
	return args.Error(0)
}

func (m *mockSession) GetState() *discordgo.State {
	args := m.Called()
	return args.Get(0).(*discordgo.State)
}

var stubLogger = mocks.NewStubLogger()

func TestDiscordMessageRepository_SendMessageInChannel(t *testing.T) {
	tests := []struct {
		name              string
		responseMessageId string
		mockReturnMessage command.ChannelCommand
		error             error
	}{
		{
			name:              "successful message send",
			responseMessageId: "someMessageId",

			error: nil,
		},
		{
			name:              "error sending message",
			responseMessageId: "",
			error:             errors.New("some error"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			testCreatedMessage := new(discordgo.Message)
			testCreatedMessage.ID = test.responseMessageId

			stubLogger := mocks.NewStubLogger()

			mockTestSession := new(mockSession)
			mockTestSession.On("ChannelMessageSend", "someChannelId", "message content", mock.Anything).Return(testCreatedMessage, test.error)

			testRepository := discord.NewMessageRepository(mockTestSession)

			messageId, err := testRepository.SendMessageInChannel("message content", "someChannelId", stubLogger)

			if test.error != err {
				t.Errorf("got error: %v, expected error: %v", err, test.error)
			}

			if strings.Compare(messageId, test.responseMessageId) != 0 {
				t.Errorf("got messageId: %s, expected: %s", messageId, test.responseMessageId)
			}
		})
	}
}

func TestDiscordMessageRepository_ReactWithEmoji(t *testing.T) {
	tests := []struct {
		name  string
		error error
	}{
		{
			name:  "successful emoji reaction send",
			error: nil,
		},
		{
			name:  "error sending emoji reaction",
			error: errors.New("some error"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			mockTestSession := new(mockSession)
			mockTestSession.On("MessageReactionAdd", "someChannelID", "someMessageID", "someEmojiID", mock.Anything).Return(test.error)

			testRepository := discord.NewMessageRepository(mockTestSession)

			err := testRepository.ReactWithEmoji("someEmojiID", "someMessageID", "someChannelID", stubLogger)

			if test.error != err {
				t.Errorf("got error: %v, expected error: %v", err, test.error)
			}

		})
	}

}
