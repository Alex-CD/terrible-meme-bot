package discord

import (
	"errors"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/Alex-CD/terrible-meme-bot/voice"
	"log/slog"
)

type voiceChannel struct {
	session Session
}

func (d *voiceChannel) GetVoiceChannel(_ string, channelID string, logger *slog.Logger) (*voice.Channel, error) {
	logger.Debug("getting user")

	channel, err := d.session.GetState().Channel(channelID)
	if err != nil {
		logger.Error(fmt.Sprintf("error occured fetching discord channel with ID %s: %s", channelID, err.Error()))
		return nil, err
	}

	if channel.Type != discordgo.ChannelTypeGuildVoice {
		return nil, errors.New(fmt.Sprintf("Attempted to fetch users of non-voice discord channel with id %s ", channelID))
	}

	memberIDs := make([]string, len(channel.Members))
	for i, member := range channel.Members {
		memberIDs[i] = member.UserID
	}

	return &voice.Channel{
		Name:    channel.Name,
		Members: memberIDs,
	}, nil
}

func NewVoiceRepository(session Session) voice.Repository {
	return &voiceChannel{
		session,
	}
}
