package discord

import (
	"github.com/bwmarrin/discordgo"
)

type ChannelMessage struct {
	session Session
	message *discordgo.MessageCreate
}

func NewChannelMessage(session Session, message *discordgo.MessageCreate) *ChannelMessage {
	return &ChannelMessage{
		session: session,
		message: message,
	}
}

func (d *ChannelMessage) ServerId() string {
	return d.message.GuildID
}

func (d *ChannelMessage) AuthorId() string {
	return d.message.Author.ID
}

func (d *ChannelMessage) ChannelId() string {
	return d.message.ChannelID
}

func (d *ChannelMessage) MessageId() string {
	return d.message.ID
}

func (d *ChannelMessage) Content() string {
	return d.message.Content
}
