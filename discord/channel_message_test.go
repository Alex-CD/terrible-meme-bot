package discord

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/Alex-CD/terrible-meme-bot/mocks"
	"strings"
	"testing"
)

func TestDiscordChannelMessage(t *testing.T) {
	mockMessageCreate := &discordgo.MessageCreate{
		Message: &discordgo.Message{
			ID:      "someMessageID",
			GuildID: "someServerID",
			Author: &discordgo.User{
				ID: "someUserID",
			},
			ChannelID: "someChannelID",
			Content:   "someMessageContent",
		},
	}
	mockSession := new(mocks.MockDiscordSession)

	message := NewChannelMessage(mockSession, mockMessageCreate)

	tests := []struct {
		name          string
		fieldGetter   func() string
		expectedValue string
	}{
		{
			name:          "ServerID",
			fieldGetter:   message.ServerId,
			expectedValue: "someServerID",
		},
		{
			name:          "AuthorID",
			fieldGetter:   message.AuthorId,
			expectedValue: "someUserID",
		},
		{
			name:          "ChannelID",
			fieldGetter:   message.ChannelId,
			expectedValue: "someChannelID",
		},
		{
			name:          "MessageID",
			fieldGetter:   message.MessageId,
			expectedValue: "someMessageID",
		},
		{
			name:          "Content",
			fieldGetter:   message.Content,
			expectedValue: "someMessageContent",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(tt *testing.T) {

			fieldValue := test.fieldGetter()
			if strings.Compare(test.expectedValue, fieldValue) != 0 {
				tt.Errorf("Discord message tests field %s: got %s want %s", test.name, fieldValue, test.expectedValue)
			}
		})
	}
}
