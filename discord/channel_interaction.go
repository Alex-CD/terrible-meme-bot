package discord

import (
	"github.com/bwmarrin/discordgo"
)

type ChannelInteraction struct {
	interaction *discordgo.InteractionCreate
}

func NewChannelInteraction(i *discordgo.InteractionCreate) *ChannelInteraction {
	return &ChannelInteraction{
		interaction: i,
	}
}

func (d ChannelInteraction) ServerId() string {
	return d.interaction.GuildID
}

func (d ChannelInteraction) AuthorId() string {
	return d.interaction.User.ID
}

func (d ChannelInteraction) ChannelId() string {
	return d.interaction.ChannelID
}
