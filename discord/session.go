package discord

import "github.com/bwmarrin/discordgo"

type Session interface {
	ChannelMessageSend(channelID string, content string, options ...discordgo.RequestOption) (*discordgo.Message, error)
	MessageReactionAdd(channelID, messageID, emojiID string, options ...discordgo.RequestOption) error
	InteractionRespond(interaction *discordgo.Interaction, resp *discordgo.InteractionResponse, options ...discordgo.RequestOption) error
	GetState() *discordgo.State
}

type session struct {
	session *discordgo.Session
}

func (s *session) ChannelMessageSend(channelID string, content string, options ...discordgo.RequestOption) (*discordgo.Message, error) {
	return s.session.ChannelMessageSend(channelID, content, options...)
}

func (s *session) MessageReactionAdd(channelID, messageID, emojiID string, options ...discordgo.RequestOption) error {
	return s.session.MessageReactionAdd(channelID, messageID, emojiID, options...)
}

func (s *session) InteractionRespond(interaction *discordgo.Interaction, resp *discordgo.InteractionResponse, options ...discordgo.RequestOption) error {
	return s.session.InteractionRespond(interaction, resp, options...)
}

func (s *session) GetState() *discordgo.State {
	return s.session.State
}

func NewSession(s *discordgo.Session) Session {
	return &session{
		session: s,
	}
}
