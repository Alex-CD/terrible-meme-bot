package discord

import (
	"github.com/bwmarrin/discordgo"
	"strings"
	"testing"
)

func TestDiscordChannelInteraction(t *testing.T) {

	mockInteractionCreate := &discordgo.InteractionCreate{
		Interaction: &discordgo.Interaction{
			ChannelID: "someChannelID",
			GuildID:   "someGuildID",
			User: &discordgo.User{
				ID: "someUserID",
			},
		},
	}

	response := NewChannelInteraction(mockInteractionCreate)

	tests := []struct {
		name          string
		fieldGetter   func() string
		expectedValue string
	}{
		{
			name:          "ChannelID",
			fieldGetter:   response.ChannelId,
			expectedValue: "someChannelID",
		},
		{
			name:          "AuthorID",
			fieldGetter:   response.AuthorId,
			expectedValue: "someUserID",
		},
		{
			name:          "ServerID",
			fieldGetter:   response.ServerId,
			expectedValue: "someGuildID",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(tt *testing.T) {

			fieldValue := test.fieldGetter()
			if strings.Compare(test.expectedValue, fieldValue) != 0 {
				tt.Errorf("DiscordInteraction field %s: got %s want %s", test.name, fieldValue, test.expectedValue)
			}
		})
	}

}
