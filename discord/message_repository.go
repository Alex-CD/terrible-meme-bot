package discord

import (
	"fmt"
	"log/slog"
)

type MessageRepository struct {
	session Session
}

func NewMessageRepository(session Session) *MessageRepository {
	return &MessageRepository{
		session: session,
	}
}

func (d *MessageRepository) SendMessageInChannel(message string, channelId string, logger *slog.Logger) (messageId string, error error) {
	logger.Debug(fmt.Sprintf("Sending discord message to channel with ID %s: '%s'", channelId, message))

	createdMessage, err := d.session.ChannelMessageSend(channelId, message)

	if err != nil {
		logger.Error(fmt.Sprintf("Error sending message to channel with ID %s", err.Error()))
		return "", err
	}

	return createdMessage.ID, err

}

func (d *MessageRepository) ReactWithEmoji(emojiId string, messageId string, channelId string, logger *slog.Logger) error {
	logger.Debug(fmt.Sprintf("Reacting with emoji id %s on message %s", emojiId, messageId))
	return d.session.MessageReactionAdd(channelId, messageId, emojiId)
}
