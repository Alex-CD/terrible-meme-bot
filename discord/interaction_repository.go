package discord

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"log/slog"
)

type InteractionResponseRepository struct {
	session     Session
	interaction *discordgo.InteractionCreate
}

func NewDiscordInteractionResponseRepository(session Session, interaction *discordgo.InteractionCreate) *InteractionResponseRepository {
	return &InteractionResponseRepository{
		session:     session,
		interaction: interaction,
	}
}

func (d InteractionResponseRepository) SendResponse(message string, logger *slog.Logger) error {
	logger.Debug(fmt.Sprintf("Sending message to channel with ID %s", message))

	return d.session.InteractionRespond(d.interaction.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: message,
		}})
}
