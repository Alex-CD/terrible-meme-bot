package initialisediscord

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/Alex-CD/terrible-meme-bot/router"
	"log"
)

func RegisterSlashCommands(s *discordgo.Session, availableCommands map[string]router.ChannelCommandDefinition, serverID string) {
	for key, v := range availableCommands {
		_, err := s.ApplicationCommandCreate(s.State.User.ID, serverID, &discordgo.ApplicationCommand{
			Name:        key,
			Description: v.Description,
		})
		if err != nil {
			log.Panicf("Cannot create '%v' command: %v", key, err)
		}
	}
}
