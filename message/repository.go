package message

import "log/slog"

type Repository interface {
	SendMessageInChannel(message string, channelId string, logger *slog.Logger) (messageId string, error error)
	ReactWithEmoji(emojiId string, messageId string, channelId string, logger *slog.Logger) error
}
