package interaction

import "log/slog"

type Repository interface {
	SendResponse(message string, logger *slog.Logger) error
}
