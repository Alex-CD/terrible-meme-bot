package main

import (
	"flag"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/google/uuid"
	"gitlab.com/Alex-CD/terrible-meme-bot/config"
	"gitlab.com/Alex-CD/terrible-meme-bot/discord"
	"gitlab.com/Alex-CD/terrible-meme-bot/handlers"
	"gitlab.com/Alex-CD/terrible-meme-bot/initialisediscord"
	"gitlab.com/Alex-CD/terrible-meme-bot/router"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
)

var logger *slog.Logger

func main() {
	logger = initLogger()
	discordInstance := initDiscord(logger)

	discordInstance.AddHandler(handleSlashCommand)
	discordInstance.AddHandler(handleIncomingMessage)

	logger.Info("Press CTRL-C to exit.")
	waitForDisconnectAndHandleClose(discordInstance)
}

func initDiscord(logger *slog.Logger) *discordgo.Session {
	tokenValue := os.Getenv("DISCORD_TOKEN")
	serverID := os.Getenv("GUILD_ID")

	discordConnection, err := discordgo.New("Bot " + tokenValue)
	if err != nil {
		logger.Error(fmt.Sprintf("Error initialising discord client: %s", err.Error()))
		os.Exit(1)
	}

	discordConnection.Identify.Intents = discordgo.IntentsAll

	err = discordConnection.Open()
	if err != nil {
		logger.Error(fmt.Sprintf("error opening connection: %s", err))
		os.Exit(1)
	}

	initialisediscord.RegisterSlashCommands(discordConnection, config.AvailableChannelCommands, serverID)

	return discordConnection
}

func initLogger() *slog.Logger {
	verbose := *flag.Bool("verbose", true, "Log level")

	logLevel := new(slog.LevelVar)
	if verbose {
		logLevel.Set(slog.LevelDebug)
	}

	return slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		Level: logLevel,
	}))
}

func handleIncomingMessage(s *discordgo.Session, i *discordgo.MessageCreate) {
	session := discord.NewSession(s)

	incomingEvent := discord.NewChannelMessage(session, i)
	messageRepository := discord.NewMessageRepository(session)
	messageRouter := router.NewMessageRouter(config.AvailableChannelCommands)
	voiceRepository := discord.NewVoiceRepository(session)

	loggerWithTraceId := logger.With("requestID", uuid.New())

	handlers.
		NewMessageHandler(messageRepository, messageRouter).
		Handle(incomingEvent, voiceRepository, loggerWithTraceId)
}

func handleSlashCommand(s *discordgo.Session, i *discordgo.InteractionCreate) {
	session := discord.NewSession(s)
	incomingEvent := discord.NewChannelInteraction(i)
	messageRepository := discord.NewMessageRepository(session)
	voiceRepository := discord.NewVoiceRepository(session)
	interactionRepository := discord.NewDiscordInteractionResponseRepository(session, i)

	loggerWithTraceId := logger.With("requestID", uuid.New())

	handlers.
		NewInteractionHandler(messageRepository, interactionRepository, config.AvailableChannelCommands).
		Handle(incomingEvent, i.ApplicationCommandData().Name, voiceRepository, loggerWithTraceId)
}

func waitForDisconnectAndHandleClose(discord *discordgo.Session) {
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc

	closeError := discord.Close()
	log.Fatal(closeError)
}
